<?php

/**
 * web-delib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib web-delib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

/**
 * [utils description]
 */
class utils
{
    public static function human_filesize($bytes, $decimals = 2)
    {
        $sz = ['B','Ko','Mo','Go'];
        $factor = floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
    }

    /**
     * Retourne un répertoire temporaire disponible dans le dossier passé en parametre
     * @param string $patchDir
     * @return bool|string
     */
    public static function newTmpDir($patchDir)
    {
        App::uses('Folder', 'Utility');
        $folder = new Folder($patchDir, true, 0777);
        //Création du répertoire temporaire par la fonction tempnam
        $outputDir = tempnam($folder->path, '');
        unlink($outputDir);
        $folder = new Folder($outputDir, true, 0777);
        return $folder->path;
    }

    /*
    * Searches for $needle in the multidimensional array $haystack.
    *
    * @param mixed $needle The item to search for
    * @param array $haystack The array to search
    * @return array|bool The indices of $needle in $haystack across the
    *  various dimensions. FALSE if $needle was not found.
    */
    public static function recursive_array_search($needle, $haystack)
    {
        foreach ($haystack as $key=>$value) {
            $current_key=$key;
            if ($needle===$value or (is_array($value) && self::recursive_array_search($needle, $value) !== false)) {
                return $current_key;
            }
        }
        return false;
    }
}
