<?php

/**
 * web-delib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib web-delib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('File', 'Utility');

/**
 * @property Typeseance Typeseance
 * @property Modeltemplate Modeltemplate
 * @property DroitsComponent Droits
 */
class ModeltemplatesController extends ModelOdtValidatorAppController
{
    public $helpers = ['Html', 'Form'];

    public $components = [
            'RequestHandler',
            'ModelOdtValidator.Fido',
            'Auth' => [
            'mapActions' => [
                'read' => ['admin_view','admin_index','admin_download'],
                'delete' => ['admin_delete'],
                'create' => ['admin_add', 'admin_getNamesFileOutputAjax'],
                'update' => ['admin_edit'],
        ]
    ]];

    public function admin_index()
    {
        $templates = $this->Modeltemplate->find('all', [
            'fields' => [
                'Modeltemplate.id',
                'Modeltemplate.name',
                'Modeltemplate.filename',
                'Modeltemplate.file_output_format',
                'Modeltemplate.modified'
            ],
            'contain' => ['Modeltype.name'],
            'order' => ['Modeltemplate.name' => 'ASC'],
            'recursive' => -1
        ]);

        foreach ($templates as &$template) {
            $id = $template['Modeltemplate']['id'];
            $template['Modeltemplate']['disabled'] = self::is_deletable($id);
        }
        $this->set('templates', $templates);

        $this->set('canEditRules', $this->Acl->check(['User' => ['id' => $this->Auth->user('id')]], 'Modelvalidations', 'read'));
    }

    public function admin_view($id = null)
    {
        if (!$id) {
            $this->Flash->set('id invalide', ['element' => 'growl','params'=>['type' => 'danger']]);
            return $this->redirect(['action' => 'index']);
        } else {
            $this->request->data = $this->Modeltemplate->find('first', [
                'fields' => [
                    'Modeltemplate.id',
                    'Modeltemplate.modeltype_id',
                    'Modeltemplate.name',
                    'Modeltemplate.content',
                    'Modeltemplate.force',
                    'Modeltemplate.filename',
                    'Modeltemplate.filesize',
                    'Modeltemplate.file_output_format',
                    'Modeltemplate.created',
                    'Modeltemplate.modified'
                ],
                'contain' => [
                    'Modeltype.name',
                    'Modeltype.description'
                ],
                'conditions' => ['Modeltemplate.id' => $id],
                'recursive' => 0
            ]);

            if (!empty($this->request->data)) {
                $Utils = new utils;
                //Conversion taille document pour lecture
                $this->request->data['Modeltemplate']['filesize'] = $Utils::human_filesize($this->request->data['Modeltemplate']['filesize']);
                //Envoi des résultats de la validation à la vue
                $this->set('validation', $this->Modeltemplate->validerFlux(
                    $this->request->data['Modeltemplate']['content'],
                    $this->request->data['Modeltemplate']['modeltype_id']
                ));
                //Vidage mémoire
                unset($this->request->data['Modeltemplate']['content']);
                return $this->render();
            } else {
                $this->Flash->set('Aucun fichier lié à ce modèle', ['element' => 'growl','params'=>['type' => 'danger']]);
                return $this->redirect(['action' => 'index']);
            }
        }
    }

    public function admin_add()
    {
        if (!empty($this->request->data)) {
            $this->_add_edit();
        }
        $modelTemplate = Configure::read('ModelOdtValidator.file.output.format.model');
        ${$modelTemplate} = ClassRegistry::init($modelTemplate);

        $this->set('aideformatOptions', ${$modelTemplate}->aideFileOutputFormat());
        $this->set('modeltypes', $this->Modeltemplate->Modeltype->find('list', ['recursive' => 0]));
        return $this->render('admin_edit');
    }

    public function admin_edit($id = null)
    {
        if (!$id) {
            $this->Flash->set('id invalide', ['element' => 'growl','params'=>['type' => 'error']]);
            return $this->redirect(['action' => 'index']);
        }

        $template = $this->Modeltemplate->find('first', [
            'conditions' => ['Modeltemplate.id' => $id],
            'fields' => [
                'id',
                'name',
                'modeltype_id',
                'filename',
                'file_output_format',
                'modified',
                'force'
            ]
        ]);

        if (!$template) {
            $this->Flash->set('Modèle introuvable', ['element' => 'growl']);
            return $this->redirect(['action' => 'index']);
        }
        if (!empty($this->request->data)) {
            $this->_add_edit($id);
        }

        $this->request->data = $template;

        $modelTemplate = Configure::read('ModelOdtValidator.file.output.format.model');
        ${$modelTemplate} = ClassRegistry::init($modelTemplate);

        $this->set('aideformatOptions', ${$modelTemplate}->aideFileOutputFormat());
        $this->set('modeltypes', $this->Modeltemplate->Modeltype->find('list', ['recursive' => 0]));
        return $this->render();
    }

    private function _add_edit($id = null)
    {
        $erreur = '';
        $this->Modeltemplate->set($this->request->data);
        if ($this->Modeltemplate->validates()) {
            if ($this->request->data['Modeltemplate']['modeltype_id'] !== MODELE_TYPE_TOUTES) {
                if (!empty($this->request->data['Modeltemplate']['fileupload'])) {
                    $validation = $this->Modeltemplate->Modeltype->Modelvalidation->validate(
                        $this->request->data['Modeltemplate']['fileupload']['tmp_name'],
                        $this->request->data['Modeltemplate']['modeltype_id'],
                        $this->request->data['Modeltemplate']['force']
                    );
                    if (empty($validation['errors'])) {
                        $this->request->data['Modeltemplate']['filename'] = $this->request->data['Modeltemplate']['fileupload']['name'];
                        $this->request->data['Modeltemplate']['filesize'] = $this->request->data['Modeltemplate']['fileupload']['size'];
                        $this->request->data['Modeltemplate']['content'] = file_get_contents($this->request->data['Modeltemplate']['fileupload']['tmp_name']);
                    }
                } else {
                    $template = $this->Modeltemplate->find('first', [
                        'recursive' => -1,
                        'conditions' => ['Modeltemplate.id' => $id],
                        'fields' => ['content']
                    ]);
                    $validation = $this->Modeltemplate->validerFlux(
                        $template['Modeltemplate']['content'],
                        $this->request->data['Modeltemplate']['modeltype_id'],
                        $this->request->data['Modeltemplate']['force']
                    );
                }

                if (!empty($validation['errors'])) {
                    $this->set('validation', $validation);
                    $erreur = "Des erreurs ont été détectées dans le modèle.";
                }
            }
        } else {
            // La logique n'est pas validée
            foreach ($this->Modeltemplate->validationErrors as $field) {
                foreach ($field as $error) {
                    $erreur .= $error . '\n';
                }
            }
        }

        if (empty($erreur)) {
            if (!empty($id)) {
                $this->Modeltemplate->id = $id;
            } else {
                $this->Modeltemplate->create();
            }

            if ($this->Modeltemplate->save($this->request->data)) {
                $this->Flash->set(__('Modèle validé et enregistré.'), ['element' => 'growl']);
                return $this->redirect(['action' => 'index']);
            } else {
                $erreur = 'Erreur lors de la sauvegarde.\nVeuillez contacter votre administrateur.';
            }
        }
        $this->Flash->set($erreur, ['element' => 'growl','params'=>['type' => 'danger']]);
        return false;
    }

    public function admin_delete($id = null)
    {
        if (empty($id)) {
            $this->Flash->set('id invalide', ['element' => 'growl','params'=>['type' => 'error']]);
            return $this->redirect(['action' => 'index']);
        } else {
            if (self::is_deletable($id)) {
                if ($this->Modeltemplate->delete($id)) {
                    $this->Flash->set('Le modèle a été supprimé.', ['element' => 'growl']);
                    return $this->redirect($this->previous);
                } else {
                    $this->Flash->set('Erreur lors de la suppression du modèle', ['element' => 'growl','params'=>['type' => 'danger']]);
                    return $this->redirect($this->previous);
                }
            } else {
                $this->Flash->set('Ce modèle ne peut pas être supprimé.', ['element' => 'growl','params'=>['type' => 'danger']]);
                return $this->redirect($this->previous);
            }
        }
    }

    /**
     * Télécharge le fichier odt du modèle
     * @param int|string $id identifiant du template
     * @return mixed
     */
    public function admin_download($id)
    {
        $template = $this->Modeltemplate->find('first', [
            'recursive' => -1,
            'conditions' => ['Modeltemplate.id' => $id],
            'fields' => [
                'Modeltemplate.id',
                'Modeltemplate.modeltype_id',
                'Modeltemplate.name',
                'Modeltemplate.content',
                'Modeltemplate.filename',
                'Modeltemplate.filesize',
                'Modeltemplate.created',
                'Modeltemplate.modified'
            ]
        ]);

        if (!empty($template)) {
            // envoi au client
            $this->response->disableCache();
            $this->response->type('application/vnd.oasis.opendocument.text');
            $this->response->body($template['Modeltemplate']['content']);
            $this->response->download($template['Modeltemplate']['filename']);
            return $this->response;
        } else {
            $this->Flash->set('Modèle introuvable', ['element' => 'growl','params'=>['type' => 'danger']]);
            return $this->redirect($this->previous);
        }
    }

    private function is_deletable($id)
    {
        //Cas particulier webdelib : test possibilité de supprimer les modèles
        if (!empty(Configure::read('ModelOdtValidator.models'))) {
            foreach (Configure::read('ModelOdtValidator.models') as $model) {
                App::uses($model, 'Model');
                $this->{$model} = ClassRegistry::init($model);
                $conditions = [];
                $columns = array_keys($this->{$model}->getColumnTypes());
                foreach (Configure::read('ModelOdtValidator.modelsType') as $type) {
                    $column_model_type_id = Inflector::underscore('Modele'.$type.'Id');
                    if (in_array($column_model_type_id, $columns, true)) {
                        $conditions['OR'][] = [$model.'.'.$column_model_type_id => $id];
                    }
                }

                if (!empty($this->{$model}->find('first', [
                    'conditions' => $conditions,
                    'recursive' => -1]))) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * [getNamesFileOutputAjax description]
     * @return [type] [description]
     */
    public function admin_getNamesFileOutputAjax($modelId)
    {
        $this->autoRender = false;
        $this->response->type('json', 'text/x-json');
        $this->RequestHandler->respondAs('json');

        $modelTemplate = Configure::read('ModelOdtValidator.file.output.format.model');
        ${$modelTemplate} = ClassRegistry::init($modelTemplate);

        $modelType = $this->Modeltype->find('first', [
            'fields' => [
                'name',
            ],
            'conditions' => ['Modeltype.id' => $modelId],
            'recursive' => -1
        ]);

        $this->response->body(
            json_encode(${$modelTemplate}->aideFileOutputFormat(Inflector::camelize(Inflector::slug($modelType['Modeltype']['name']))))
        );

        return $this->response;
    }
}
