<?php

/**
 * web-delib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib web-delib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

/**
 * [ModelsectionsController description]
 */
class ModelsectionsController extends ModelOdtValidatorAppController
{
    public $components = [
            'Auth' => [
            'mapActions' => [
                'allow' => ['admin_index','admin_add','admin_delete','admin_edit','admin_view'],
        ]
    ]];

    public function admin_edit($id = null)
    {
        if (!empty($id)) {
            if (empty($this->request->data)) {
                $this->request->data = $this->Modelsection->find('first', ['conditions' => ['Modelsection.id' => $id]]);
            } else {
                $conditions = [
                    'NOT' => ['Modelsection.id' => $id],
                    'Modelsection.name' => $this->request->data['Modelsection']['name'],
                ];
                if (!$this->Modelsection->hasAny($conditions)) {
                    $this->Modelsection->id = $id;
                    if (empty($this->request->data['Modelsection']['parent_id'])) {
                        $this->request->data['Modelsection']['parent_id'] = 1;
                    }
                    if ($this->Modelsection->save($this->request->data)) {
                        $this->Flash->set("Section enregistrée avec succès.", ['element' => 'growl','params'=>['type' => 'success']]);
                        return $this->redirect($this->previous);
                    } else {
                        $this->Flash->set("Erreur lors de l'enregistrement de la section.", ['element' => 'growl','params'=>['type' => 'error']]);
                    }
                } else {
                    $this->Flash->set("Une section de ce nom existe déjà.", ['element' => 'growl','params'=>['type' => 'danger']]);
                }
            }
        } else {
            $this->Flash->set("Veuillez spécifier la section à modifier", 'growl');
            return $this->redirect(['action' => 'index']);
        }
        $parents = $this->Modelsection->find('list', [
            'order' => ['name ASC'],
            'conditions' => ['not'=> ['id' => [$id, 1]]]
        ]);
        $this->set('parents', $parents);
    }

    public function admin_add()
    {
        if (!empty($this->request->data)) {
            $conditions = [
                'Modelsection.name' => $this->request->data['Modelsection']['name'],
            ];
            if (!$this->Modelsection->hasAny($conditions)) {
                $maxid = $this->Modelsection->find('first', [
                    'recursive' => -1,
                    'fields' => ['id'],
                    'order' => ['id DESC']
                ]);
                $this->request->data['Modelsection']['id'] = $maxid['Modelsection']['id'] + 1;
                if (empty($this->request->data['Modelsection']['parent_id'])) {
                    $this->request->data['Modelsection']['parent_id'] = 1;
                }
                $this->Modelsection->create();
                if ($this->Modelsection->save($this->request->data)) {
                    $this->Flash->set("Section créée avec succès.", ['element' => 'growl','params'=>['type' => 'success']]);
                    return $this->redirect($this->previous);
                } else {
                    $this->Flash->set("Erreur lors de l'enregistrement des modifications.", ['element' => 'growl','params'=>['type' => 'error']]);
                }
            } else {
                $this->Flash->set("Une section de ce nom existe déjà.", 'growl');
            }
        }
        $parents = $this->Modelsection->find('list', [
            'order' => ['name ASC'],
            'conditions' => ['not'=> ['id' => 1]]
        ]);
        $this->set('parents', $parents);
        $this->render('admin_edit');
    }

    public function admin_index()
    {
        $conditions = [];
        if (!empty($this->request->data['Modelsection']['nom'])) {
            $conditions['Modelsection.id'] = $this->request->data['Modelsection']['nom'];
        } else {
            $conditions['Modelsection.id >'] = 1;
        }

        $data = $this->Modelsection->find('all', [
            'conditions' => $conditions,
            'contain' => ['Parent'],
            'order' => 'Modelsection.name ASC'
        ]);
        $this->set('data', $data);
        $this->set('names', $this->Modelsection->find('list'));
    }

    public function admin_delete($id)
    {
        if ($this->Modelsection->delete($id, false)) {
            $this->Flash->set("La section $id à été supprimée", 'growl');
        } else {
            $this->Flash->set("Impossible de supprimer la section $id", 'growl');
        }
        return $this->redirect($this->previous);
    }
}
