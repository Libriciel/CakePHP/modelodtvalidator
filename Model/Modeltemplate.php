<?php

/**
 * web-delib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib web-delib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('ModelOdtValidatorAppModel', 'ModelOdtValidator.Model');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

/**
 * [Modeltemplate description]
 * @property Fido Fido
 * @property Modeltype Modeltype
 */
class Modeltemplate extends ModelOdtValidatorAppModel
{
    public $recursive = 0;
    //Relations
    public $belongsTo = [
        'Modeltype' => [
            'className' => 'ModelOdtValidator.Modeltype',
            'foreignKey' => 'modeltype_id'
        ]
    ];
    //Validations
    public $validate = [
        'name' => [
            'rule' => 'notBlank',
            'required' => true,
            'allowEmpty' => false,
            'message' => 'Veuillez attribuer un nom au modèle.'
        ],
        'modeltype_id' => [
            [
                'rule' => 'notBlank',
                'required' => true,
                'allowEmpty' => false,
                'message' => 'Veuillez attribuer un type au modèle.'
            ],
            [
                'rule' => 'numeric',
                'required' => true,
                'allowEmpty' => false,
                'message' => 'Veuillez sélectionner un type valide.'
            ]
        ],
        'fileupload' => [
            'rule' => [
                'extension',
                ['odt']
            ],
            'required' => false,
            'allowEmpty' => false,
            'message' => 'Erreur de fichier. Document ODT attendu.'
        ]
    ];

    /**
     * Validation du format du fichier par FIDO
     */
    public function checkFormat($data, $extension = null, $required = false)
    {
        $data = array_shift($data);
        if ($data['size'] == 0 || $data['error'] != 0) {
            return false;
        }
        $this->Fido = new FidoComponent();
        $file = new File($data['tmp_name'], false);
        $folder_temp = new Folder(utils::newTmpDir(TMP . DS . 'files' . DS . 'upload'));
        $file_test = new File($folder_temp->pwd() . DS . $data['name']);
        $file_test->write($file->read());
        $allowed = $this->Fido->checkFile($file_test->pwd());
        $allowed = $this->Fido->checkFile($data['tmp_name']);
        $results = $this->Fido->lastResults;
        $folder_temp->delete();

        return ($allowed && $results['mimetype'] == 'application/vnd.oasis.opendocument.text');
    }

    /**
     * Récupère des les modèles pour un type donné
     * @since 1.0.0
     * @version 2.0.1
     * @param string $types
     * @param bool $includeToutesEditions inclure les modèles de type "Toutes éditions"
     * @return array|false
     */
    public function getModels($types, $includeToutesEditions = false)
    {
        $modeltype_ids = [];
        if (!is_array($types)) {
            array_push($modeltype_ids, $types);
        } else {
            $modeltype_ids = $types;
        }

        if ($includeToutesEditions) {
            array_push($modeltype_ids, MODELE_TYPE_TOUTES);
        }

        return $this->find('list', [
                    'conditions' => [
                        'modeltype_id' => $modeltype_ids
                    ]
        ]);
    }

    /**  @deprecated since version 2.0.1
     * Récupère tous les modèles associés aux types voulus
     * @param array $types types de modèles à récupérer
     * @return array
     */
    public function getModelsByTypes($types)
    {
        return $this->find('list', [
                    'conditions' => [
                        'modeltype_id' => $types
                    ]
        ]);
    }

    /**
     * @param string $content contenu du document odt
     * @return File instance de la classe File
     */
    public function createTmpFile($content)
    {
        //Création fichier temporaire
        App::uses('Folder', 'Utility');
        $folder = new Folder(TMP . 'files' . DS . 'modelodtvalidator', true, 0777);
        $outputDir = tempnam($folder->path, '');
        unlink($outputDir);
        $file = new File($outputDir . '.odt', true, 0755);
        $file->write($content);
        return $file;
    }

    /**
     * @param string $content
     * @param int $modeltype_id
     * @return mixed
     */
    public function validerFlux($content, $modeltype_id, $force = false)
    {
        $file = $this->createTmpFile($content);
        //Analyse du document
        $validation = $this->Modeltype->Modelvalidation->validate($file->path, $modeltype_id, $force);
        //Suppression du fichier
        $file->delete();
        return $validation;
    }
}
