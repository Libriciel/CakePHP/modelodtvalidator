<?php

/**
 * web-delib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib web-delib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('ModelOdtValidatorAppModel', 'ModelOdtValidator.Model');

/**
 * [Modelvariable description]
 */
class Modelvariable extends ModelOdtValidatorAppModel
{
    public $hasMany = ['ModelOdtValidator.Modelvalidation'];
    public $order = "name";

    /**
     * @var array Règles de validation des enregistrements
     */
    public $validate = [
        'name' => [
            'isUnique' => [
                'rule' => 'isUnique',
                'allowEmpty' => false,
                'message' => 'Ce nom est déjà utilisé.'
            ],
            'maxLength' => [
                'rule' => ['maxLength', 255],
                'message' => 'Le nom ne doit pas dépasser 255 caractères.'
            ]
        ],
        'description' => [
            'rule' => ['maxLength', 255],
            'message' => 'La description ne doit pas dépasser 255 caractères.'
        ]
    ];

    /**
     * Trouve l'id d'une variable par son nom
     * @param string $name nom de la variable
     * @return integer id de la variable
     */
    public function findIdByName($name)
    {
        $modelvariable = $this->find('first', [
            'recursive' => -1,
            'fields' => ['id'],
            'conditions' => ['Modelvariable.name' => $name]
        ]);
        if (empty($modelvariable['Modelvariable']['id'])) {
            return false;
        }
        return $modelvariable['Modelvariable']['id'];
    }

    /**
     * La variable est elle autorisée pour le type
     * @param string $variable
     * @param integer $idType
     * @return bool
     */
    public function autoriseePourType($variable, $idType)
    {
        if ($idType == MODELE_TYPE_TOUTES) {
            return true;
        }
        $variableId = $this->findIdByName($variable);
        if (!$variableId) {
            return true;
        }

        $exist = $this->Modelvalidation->find('count', [
            'conditions' => [
                'modelvariable_id' => $variableId,
                'modeltype_id' => $idType
            ]
        ]);

        return (!empty($exist));
    }

    /**
     * La variable est elle autorisée dans la section pour le type (ou le document)
     * @param string $variable
     * @param integer $idType
     * @param string $section
     * @return bool
     */
    public function autoriseePourSection($variable, $idType, $section)
    {
        if ($idType == MODELE_TYPE_TOUTES) {
            return true;
        }
        $variableId = $this->findIdByName($variable);
        if (!$variableId) {
            return true;
        }

        $sectionId = $this->Modelvalidation->Modelsection->findIdByName($section);
        if (!$sectionId) {
            return true;
        }

        $exist = $this->Modelvalidation->find('count', [
            'conditions' => [
                'modelvariable_id' => $variableId,
                'modelsection_id' => [$sectionId, MODEL_SECTION_DOCUMENT],
                'modeltype_id' => $idType
            ]
        ]);

        return (!empty($exist));
    }

    /**
     * @param string $variable nom de la variable
     * @param integer $idType identifiant du type de model
     * @param string $nombre a comparer avec min et max
     * @return bool le nombre est compris entre min et max
     */
    public function checkMultiplicite($variable, $idType, $nombre)
    {
        if ($idType == MODELE_TYPE_TOUTES) {
            return true;
        }
        $variableId = $this->findIdByName($variable);
        if (!$variableId) {
            return true;
        }

        $record = $this->Modelvalidation->find('first', [
            'fields' => ['min', 'max'],
            'conditions' => [
                'modelvariable_id' => $variableId,
                'modeltype_id' => $idType
            ]
        ]);

        return ($record['Modelvalidation']['min'] <= $nombre || empty($record['Modelvalidation']['min']))
        && ($nombre <= $record['Modelvalidation']['max'] || empty($record['Modelvalidation']['max']));
    }

    /**
     * @param string $variable nom de la variable
     * @param integer $idType identifiant du type de model
     * @param string $section nom de la section
     * @param integer $nombre a comparer avec min et max
     * @return bool le nombre est compris entre min et max
     */
    public function checkMultipliciteDansSection($variable, $idType, $section, $nombre)
    {
        //Type "Toutes Editions" : pas d'erreur
        if ($idType == MODELE_TYPE_TOUTES) {
            return true;
        }
        $variableId = $this->findIdByName($variable);
        //La variable n'est pas répertoriée : pas d'erreur
        if (!$variableId) {
            return true;
        }
        $sectionId = $this->Modelvalidation->Modelsection->findIdByName($section);
        //La section n'est pas répertoriée : pas d'erreur
        if (!$sectionId) {
            return true;
        }

        $record = $this->Modelvalidation->find('first', [
            'fields' => ['min', 'max'],
            'conditions' => [
                'modelvariable_id' => $variableId,
                'modelsection_id' => $sectionId,
                'modeltype_id' => $idType
            ]
        ]);

        if (empty($record)) {
            $record = $this->Modelvalidation->find('first', [
                'fields' => ['min', 'max'],
                'conditions' => [
                    'modelvariable_id' => $variableId,
                    'modelsection_id' => MODEL_SECTION_DOCUMENT,
                    'modeltype_id' => $idType
                ]
            ]);
        }

        if (empty($record)) {
            return false;
        }

        return ($record['Modelvalidation']['min'] <= $nombre || empty($record['Modelvalidation']['min']))
        && ($nombre <= $record['Modelvalidation']['max'] || empty($record['Modelvalidation']['max']));
    }

    /**
     * @param string $name nom de la variable dont on vérifie la présence
     * @return bool le nom de variable est présent
     */
    public function checkPresence($name)
    {
        $query = [
            'fields' => [ "{$this->alias}.{$this->primaryKey}" ],
            'conditions' => [
                "{$this->alias}.{$this->displayField}" => $name
            ],
            'contain' => false,
            'recursive' => -1
        ];

        $result = $this->find('first', $query);

        return true === empty($result);
    }
}
