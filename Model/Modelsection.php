<?php

/**
 * web-delib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib web-delib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('ModelOdtValidatorAppModel', 'ModelOdtValidator.Model');

/**
 * [Modelsection description]
 */
class Modelsection extends ModelOdtValidatorAppModel
{

    /*
     * Relations entre modèles
     */
    public $hasMany = [
        'Modelvalidation' => [
            'className' => 'ModelOdtValidator.Modelvalidation'
        ]
    ];
    public $belongsTo = [
        'Parent' => [
            'className' => 'ModelOdtValidator.Modelsection',
            'foreignKey' => 'parent_id'
        ]
    ];

    /**
     * @var array Règles de validation des enregistrements
     */
    public $validate = [
        'name' => [
            'isUnique' => [
                'rule' => 'isUnique',
                'allowEmpty' => false,
                'message' => 'Ce nom est déjà utilisé.'
            ],
            'maxLength' => [
                'rule' => ['maxLength', 255],
                'message' => 'Le nom ne doit pas dépasser 255 caractères.'
            ]
        ],
        'description' => [
            'rule' => ['maxLength', 255],
            'message' => 'La description ne doit pas dépasser 255 caractères.'
        ]
    ];

    /**
     * Trouve l'id d'une section par son nom
     * @param string $name nom de la section
     * @return integer id de la section
     */
    public function findIdByName($name)
    {
        $modelsection = $this->find('first', [
            'recursive' => -1,
            'fields' => ['id'],
            'conditions' => ['Modelsection.name' => $name]
        ]);
        if (empty($modelsection)) {
            return false;
        }
        return $modelsection['Modelsection']['id'];
    }

    /**
     * la section est elle autorisée à exister dans ce type de modèle
     * @param string $section nom de la section
     * @param integer $idType identifiant du type de modèle
     * @return bool
     */
    public function autoriseePourType($section, $idType)
    {
        if ($idType == MODELE_TYPE_TOUTES) {
            return true;
        }
        $sectionId = $this->findIdByName($section);
        if (!$sectionId) {
            return true;
        }

        $exist = $this->Modelvalidation->find('count', [
            'conditions' => [
                'modelvariable_id' => null,
                'modelsection_id' => $sectionId,
                'modeltype_id' => $idType
            ]
        ]);

        return (!empty($exist));
    }

    /**
     * la section apparait un nombre autorisé de fois dans le document
     * @param string $section nom de la section
     * @param integer $idType identifiant du type de modèle
     * @param integer $nombre nombre de fois que la section apparait
     * @return bool
     */
    public function checkMultiplicite($section, $idType, $nombre)
    {
        if ($idType == MODELE_TYPE_TOUTES) {
            return true;
        }
        $sectionId = $this->findIdByName($section);
        if (!$sectionId) {
            return true;
        }

        $record = $this->Modelvalidation->find('first', [
            'fields' => ['Modelvalidation.min', 'Modelvalidation.max'],
            'conditions' => [
                'Modelvalidation.modelvariable_id' => null,
                'Modelvalidation.modelsection_id' => $sectionId,
                'Modelvalidation.modeltype_id' => $idType
            ]
        ]);
        if (empty($record)) {
            return true;
        }

        return ($record['Modelvalidation']['min'] <= $nombre || empty($record['Modelvalidation']['min']))
        && ($nombre <= $record['Modelvalidation']['max'] || empty($record['Modelvalidation']['max']));
    }


    /**
     * la section est elle autorisée à exister dans cette section parent
     * @param string $section nom de la section
     * @param integer $parent nom de la section parent
     * @return bool
     */
    public function autoriseePourSection($section, $parent)
    {
        $parentId = $this->findIdByName($parent);
        if (!$parentId) {
            return true;
        }

        $exist = $this->find('count', [
            'conditions' => [
                'name' => $section,
                'parent_id' => $parentId
            ]
        ]);

        return (!empty($exist));
    }
}
