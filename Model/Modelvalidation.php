<?php

/**
 * web-delib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib web-delib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('ModelOdtValidatorAppModel', 'ModelOdtValidator.Model');

/**
 * [Modelvalidation description]
 */
class Modelvalidation extends ModelOdtValidatorAppModel
{
    public $order = ['Modelvalidation.modeltype_id' => 'asc', 'Modelvalidation.modelsection_id' => 'asc', 'Modelvalidation.modelvariable_id' => 'asc'];
    public $belongsTo = [
        'Modeltype' => [
            'className' => 'ModelOdtValidator.Modeltype',
            'foreignKey' => 'modeltype_id'
        ],
        'Modelvariable' => [
            'className' => 'ModelOdtValidator.Modelvariable',
            'foreignKey' => 'modelvariable_id'
        ],
        'Modelsection' => [
            'className' => 'ModelOdtValidator.Modelsection',
            'foreignKey' => 'modelsection_id'
        ],
    ];

    /**
     * @var array Règles de validation du modèle
     */
    public $validate = [
        'id' => [
            'rule' => 'blank',
            'on' => 'create'
        ],
        'min' => [
            'rule' => 'numeric',
            'allowEmpty' => true,
            'message' => 'Merci de soumettre le nombre minimum d\'occurences'
        ],
        'max' => [
            'rule' => 'numeric',
            'allowEmpty' => true,
            'message' => 'Merci de soumettre le nombre maximum d\'occurences'
        ],
        'actif' => [
            'rule' => ['boolean'],
            'message' => 'Valeur incorrecte pour actif'
        ]
    ];

    /**
     * [validate Validation du modèle lors de l'ajout/modification d'un modèle]
     * @param  [type] $file   [description]
     * @param  [type] $idType [description]
     * @param  [type] $force  [description]
     * @return [type]         [description]
     */
    public function validate($file, $idType, $force = false)
    {
        /*
         * Initialisations
         */
        //Variable de retour
        $validation = [
            'warnings' => [],
            'errors' => []
        ];
        //Initialisation de la librairie
        $this->phpOdtApi = new phpOdtApi;
        try {
            $this->phpOdtApi->loadFromFile($file);
        } catch (Exception $e) {
            $validation['errors'][] = $e->getMessage();
            return $validation;
        }

        //Validation des sections
        $sections = $this->phpOdtApi->getSections();
        foreach ($sections as $section) {
            //La section existe ?
            if ($this->Modelsection->findIdByName($section)) {
                if ($this->Modelsection->autoriseePourType($section, $idType)) {
                    $nombre = $this->phpOdtApi->countSection($section);
                    if (!$this->Modelsection->checkMultiplicite($section, $idType, $nombre)) {
                        $validation['errors'][] = "La section '$section' n'est pas autorisée à apparaitre $nombre fois pour ce type de modèle";
                    }
                    //TODO valider la section parent
//                    $parent_section = $this->phpOdtApi->getParentSection($section);
//                    if (!$this->Modelsection->autoriseePourSection($section, $parent_section))
//                        $validation['errors'][] = "La section '$section' n'est pas autorisée à être dans la section '$parent_section'";
                } else {
                    $validation['errors'][] = "La section '$section' n'est pas autorisée pour ce type de modèle";
                }
            } else {
                $validation['warnings'][] = "La section '$section' est inconnue";
            }
        }
        //Validation des variables
        $variables = $this->phpOdtApi->getUserFields();
        foreach ($variables as $variable) {
            //La variable existe ?
            if ($this->Modelvariable->findIdByName($variable)) {
                if ($this->Modelvariable->autoriseePourType($variable, $idType)) {
                    $nombreTotal = $this->phpOdtApi->countUserFields($variable);

                    if (!$this->Modelvariable->checkMultiplicite($variable, $idType, $nombreTotal)) {
                        $validation['errors'][] = "La variable '$variable' n'est pas autorisée à apparaitre $nombreTotal fois pour ce type de modèle";
                    }

                    $containers = $this->phpOdtApi->getUserFieldSectionContainer($variable);
                    foreach ($containers as $section) {
                        $nombreEnSection = $this->phpOdtApi->countUserFieldsInSection($variable, $section);
                        if ($this->Modelvariable->autoriseePourSection($variable, $idType, $section)) {
                            if (!$this->Modelvariable->checkMultipliciteDansSection($variable, $idType, $section, $nombreEnSection)) {
                                $validation['errors'][] = "La variable '$variable' n'est pas autorisée à apparaitre $nombreEnSection fois dans la section '$section' pour ce type de modèle";
                            }
                        } else {
                            $validation['errors'][] = "La variable '$variable' n'est pas autorisée à apparaitre dans la section '$section' pour ce type de modèle";
                        }
                    }
                } else {
                    $validation['errors'][] = "La variable '$variable' n'est pas autorisée pour ce type de modèle";
                }
            } // fin : la variable existe
            elseif (Configure::read('APP_CONTAINER') == 'WEBDELIB') { // variable introuvable
                /*
                 * Model Infosupdef issue de WEBDELIB
                 */
                App::uses('Infosupdef', 'Model');
                $this->Infosupdef = new Infosupdef();
                //TODO Affiner le filtre pour la recherche d'infosup (bon type, ...)
                //Est-ce une info sup ?
                $count = $this->Infosupdef->find('count', [
                    'conditions' => ['code' => $variable]
                ]);
                if (empty($count)) {
                    $validation['warnings'][] = "La variable '$variable' est inconnue";
                }
            } // fin : variable introuvable
        }

        foreach ($this->phpOdtApi->getVariablesDeclared() as $variable) {
            $validation['warnings'][] = "La variable '$variable' est déclarée comme 'variable' et non comme 'champ utilisateur'";
        }

//        foreach ($this->phpOdtApi->getUserFieldsNotUsed() as $notUsed) {
//            $validation['warnings'][] = "La variable '$notUsed' est déclarée mais n'est pas utilisée";
//        }
        if ($force) {
            $validation['errors'] = [];
        }
        return $validation;
    }

    public function getXmlFromFile($path)
    {
        $this->phpOdtApi = new phpOdtApi();
        $file = new File($path, false);
        if ($file->exists()) {
            $this->phpOdtApi->loadFromFile($file->path);
            return $this->phpOdtApi->content;
        } else {
            throw new Exception('Le fichier modèle n\'existe pas');
        }
    }
}
