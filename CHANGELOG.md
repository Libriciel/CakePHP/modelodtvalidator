# Change Log

Toutes les modifications apportées au projet seront documentées dans ce fichier.

Le format est basé sur le modèle [Keep a Changelog](http://keepachangelog.com/) et adhère aux principes du [Semantic Versioning](http://semver.org/).

## [3.0.0] - 2024-01-05

### Evolutions
- Changement de la license open source vers MIT
- Prise en charge de la diffusion avec packagist

## [2.3.2] - 2023-02-06

### Correction
- Les modèles ne sont plus validé par FIDO

## [2.3.1] - 2020-06-26

### Correction
- Génération du schéma de base pour validation

## [2.3] - 2020-06-16

### Evolutions
- Pouvoir forcer l'upload d'un modèle
- Paramétrage des noms de fichier des sorties de génération
- Migration vers Font Awesome 5

## [2.2.2] - 2019-06-20

### Evolutions
- Prise en compte des droits CRUD sur les modèles

## [2.2.1] - 2019-02-06

### Correction
- Génération du schéma de base pour validation

## [2.2.0] - 2019-01-31

### Ajout
- Utilisation de l'historique pour la navigation
- Vérification de la présence d'un nom de variable ~Core

## [2.1.0] - 2017-06-29

### Ajout
- Nouvelle vérification de variable ~Core

### Evolutions
- Amélioration de la vérification de variable ~Core

### Corrections
- Support PHP 7 ~Core
- Recherche récursive dans les modèles ~Core

## [2.0.1] - 2017-04-12

### Corrections
- Visualisation d'un modèle

## [2.0.0] - 2017-03-29

### Evolutions
- Support PHP 7 et FIDO 1.3.5 ~Core

## [1.1.2] - 2016-07-15

### Corrections
- Suppression des transactions des script sql ~Database

## [1.1.1]

### Corrections
- Gestion lorsque le mimetype sur Fido est non disponible

## [1.1.0]

### Supressions
- Suppression des données liés à web-delib

## [1.0.1]

### Evolutions
- Prise en compte des variables nombre_annexes dans des les modèles
- Prise en compte de la section Annexes dans des les modèles

### Corrections
- Correction : Gestion des retours de fido avec des résultats multiples

## [1.0.0]

### Ajouts
- Sortie officielle du plugin CakePHP ModelOdtValidator

[2.2.1]: https://gitlab.libriciel.fr/CakePHP/ModelOdtValidator/tags/2.0.0...2.2.1
[2.2.0]: https://gitlab.libriciel.fr/CakePHP/ModelOdtValidator/tags/2.1.0...2.2.0
[2.1.0]: https://gitlab.libriciel.fr/CakePHP/ModelOdtValidator/tags/2.0.1...2.1.0
[2.0.1]: https://gitlab.libriciel.fr/CakePHP/ModelOdtValidator/tags/2.0.0...2.0.1
[2.0.0]: https://gitlab.libriciel.fr/CakePHP/ModelOdtValidator/tags/1.1.2...2.0.0
[1.1.2]: https://gitlab.libriciel.fr/CakePHP/ModelOdtValidator/tags/1.1.1...1.1.2
[1.1.1]: https://gitlab.libriciel.fr/CakePHP/ModelOdtValidator/tags/1.1.0...1.1.1
[1.0.1]: https://gitlab.libriciel.fr/CakePHP/ModelOdtValidator/tags/1.0.1...1.1.0
[2.0.0]: https://gitlab.libriciel.fr/CakePHP/ModelOdtValidator/tags/1.0.0...1.0.1
[1.1.2]: https://gitlab.libriciel.fr/CakePHP/ModelOdtValidator/tags/1.0.0
