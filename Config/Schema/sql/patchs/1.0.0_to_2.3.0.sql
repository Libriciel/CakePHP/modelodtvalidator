ALTER TABLE IF EXISTS modeltemplates ADD COLUMN file_output_format varchar(255);
ALTER TABLE IF EXISTS modeltemplates ADD COLUMN force boolean;
