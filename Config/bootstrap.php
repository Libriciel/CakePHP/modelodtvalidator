<?php
/**
 * Routing Prefix
 * Set the prefix you would like to restrict the plugin to
 * @see Configure::read('Routing.prefixes')
 */
 Configure::write('Routing.prefixes', array('admin', 'manager'));


/**
 * Fichier de configuration du plugin ModelOdtValidator
 */

/**
 * ModelOdtValidator.type : identifiants des types de modèle
 */
define('MODELE_TYPE_TOUTES', 1);
foreach (Configure::read('ModelOdtValidator.modelsType') as $key => $value) {
    define('MODELE_TYPE_' . strtoupper(Inflector::underscore($value)), $key+2);
}
/**
 * ModelOdtValidator.section : identifiants des sections de modèle
 */
 define('MODEL_SECTION_DOCUMENT', 1);
foreach (Configure::read('ModelOdtValidator.modelsSection') as $key => $value) {
    define('MODELE_SECTION_' . strtoupper(Inflector::underscore($value)), $key+2);
}

/**
* @deprecated
* @var bool Manage parapheur signature
*/
//Configure::write('ModelOdtValidator.file.output.format.model', 'Template');

/**
 * Définit dans quelle application est intégré le plugin
 * (gestion des cas particuliers)
 *
 */
Configure::write('APP_CONTAINER', Configure::write('ModelOdtValidator.appContainer'));
