<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Maintenance'));
$this->Html->addCrumb(__('Règles de validation des modèles'), ['admin' => true,'prefix'=> 'admin', 'plugin'=>'model_odt_validator', 'controller'=>'modelvalidations', 'action'=>'index']);

$this->Html->addCrumb('Sections de modèles');
?>

<h2>Sections de modèles</h2>
<br/>
<?php
echo $this->Form->create('Modelsection', ['url'=>['action' => 'index']]);
echo $this->Form->input('nom', ['label' => ['text' => 'Rechercher une section', 'style' => 'width: auto; padding-top: 5px;'], 'placeholder' => 'Nom de section', 'options' => $names, 'empty' => true, 'div' => false]);
echo '<br />';
echo $this->Form->button('<i class="fa fa-filter"></i> Appliquer le filtre', ['class' => 'btn btn-info', 'escape' => false, 'type' => 'submit', 'style' => 'margin-left:5px']);
echo $this->Html->tag('hr');
echo $this->Form->end();

$barreActions = $this->Html->tag('div', null, ['class' => 'btn-group']);
$barreActions .= $this->Html->link('<i class="fa fa-flag-checkered"></i> Ajouter une section', ['action' => 'add'], ['escape' => false, 'class' => 'btn btn-primary', 'title' => 'Déclarer une nouvelle section de modèle']);
$barreActions .= $this->Html->link('<i class="fa fa-flag"></i> Règles de validation', ['controller' => 'Modelvalidations'], ['escape' => false, 'class' => 'btn btn-info', 'title' => 'Règles de validation des modèles d\'édition']);
$barreActions .= $this->Html->link('<i class="fa fa-code"></i> Variables', ['controller' => 'Modelvariables'], ['escape' => false, 'class' => 'btn btn-info', 'title' => 'Liste des variables de modèle']);
$barreActions .= $this->Html->tag('/div', null);

echo $barreActions;
?>
<hr/>
<table class="table table-striped">
    <caption>Liste des sections autorisées dans les modèles</caption>
    <thead>
    <tr>
        <th>id</th>
        <th>Nom</th>
        <th>Description</th>
        <th>Parent</th>
        <th>Création</th>
        <th>Modification</th>
        <th style="width: 140px">Actions</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($data as $section) {
        echo $this->Html->tag('tr', null);
        echo $this->Html->tag('td', $section['Modelsection']['id']);
        echo $this->Html->tag('td', $section['Modelsection']['name']);
        echo $this->Html->tag('td', $section['Modelsection']['description']);
        echo $this->Html->tag('td', $section['Parent']['id'] > 1 ? $this->Html->link($section['Parent']['name'], ['action' => 'edit', $section['Parent']['id']]) : '');
        echo $this->Html->tag('td', date('d/m/Y \à h:i:s', strtotime($section['Modelsection']['created'])));
        echo $this->Html->tag('td', date('d/m/Y \à h:i:s', strtotime($section['Modelsection']['modified'])));
        echo $this->Html->tag(
            'td',
            $this->Bs->div('btn-group')
            .
            $this->Bs->btn($this->Bs->icon('pencil-alt'), ['action' => 'edit', $section['Modelsection']['id']], ['type' => 'primary', 'escapeTitle' => false, 'title' => 'Modifier la règle'])
            .
            $this->Bs->btn($this->Bs->icon('trash'), ['action' => 'delete', $section['Modelsection']['id']], ['type' => 'danger', 'escapeTitle' => false, 'title' => 'Supprimer la règle'], "Confirmez-vous la suppression de cette règle de validation ? Attention cette action est irréversible !")
            .
            $this->Bs->close()
        );
        echo $this->Html->tag('/tr', null);
    }
    ?>
    </tbody>
</table>
<hr/>
<?php
echo $barreActions;
?>
<script type="application/javascript">
    require(['domReady'], function (domReady) {
    domReady(function () {
        $('#filtrer').change(function () {
            if ($('#filtrer').prop('checked'))
                $('#divFiltres').show();
            else
                $('#divFiltres').hide();
        });

        $('#ModelsectionNom').select2({
            width: 'element',
            placeholder: "Nom de section",
            allowClear: true
        });

        <?php if (!empty($this->request->data['Modelsection']['filtrer'])) : ?>
        $('#divFiltres').show();
        <?php endif; ?>

    });
});
</script>
