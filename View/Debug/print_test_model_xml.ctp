<?php
/**
 * Created by PhpStorm.
 * User: fajir
 * Date: 19/11/13
 * Time: 15:54
 */

echo $this->Html->tag('h2', 'Contenu XML de document ODT');
echo $this->Form->create('Odt', ['type' => 'file']);
echo $this->Form->input('file', ['type' => 'file']);
echo $this->Form->input('dl', ['type' => 'checkbox']);
echo $this->Form->end('Afficher XML');
if (!empty($xml)) {
    echo $this->Form->input('xml', ['type' => 'textarea', 'value' => $xml]);
}

?>
<style>
    #xml{
        width: 100%;
        height: 600px;
    }
</style>
