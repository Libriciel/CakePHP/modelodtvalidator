<?php

/**
 * web-delib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib web-delib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

if (empty($sql)) {
    echo $this->Html->tag('h2', 'Convertisseur CSV -> SQL');
    echo $this->Form->create('Csv', ['type' => 'file']);
    echo $this->Form->input('file', ['type' => 'file']);
    echo $this->Form->end('Convertir');
}
