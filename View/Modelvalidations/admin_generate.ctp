<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Maintenance'));
$this->Html->addCrumb(__('Règles de validation des modèles'), ['admin' => true,'prefix'=> 'admin', 'plugin'=>'model_odt_validator', 'controller'=>'modelvalidations', 'action'=>'index']);
$this->Html->addCrumb(__('Création d\'une règle de validation de modèle d\'édition'));

echo $this->Bs->tag('h3', __('Génération de nouvelles règles de validation')) ;

echo $this->BsForm->create('Modelvalidation', ['url' => [], 'type' => 'post']);
echo $this->BsForm->select('modelvariable_id', $variables, ['label' => 'Variable(s) concernée(s)','class'=>'selectone', 'empty' => true, 'multiple' => true]);
echo $this->BsForm->select('modelsection_id', $sections, ['label' => 'Section(s) concernée(s)', 'class'=>'selectone','empty' => true, 'multiple' => true]);
echo $this->BsForm->select('modeltype_id', $types, ['label' => 'Type(s) concerné(s)', 'class'=>'selectone', 'empty' => true, 'multiple' => true]);
echo $this->BsForm->checkbox('activeMin', ['label' => 'Fixer un nombre minimum', 'value' => false]);
echo $this->BsForm->input('min', ['label' => 'Minimum', 'type' => 'number', 'value' => '0', 'div' => ['style' => 'display:none']]);
echo $this->BsForm->checkbox('activeMax', ['label' => 'Fixer un nombre maximum', 'value' => false]);
echo $this->BsForm->input('max', ['label' => 'Maximum', 'type' => 'number', 'value' => '0', 'div' => ['style' => 'display:none']]);
echo $this->Bs->div(
    'sql_bloc',
    $this->Form->input('sql_commands', ['label' => false, 'type' => 'textarea', 'id' => 'sql_commands', 'readonly' => true]),
    []
);

echo $this->Html2->btnSaveCancel();
echo $this->Form->end();
?>
<script type="application/javascript">
require(['domReady'], function (domReady) {
    domReady(function () {

        $('#ModelvalidationModeltypeId').select2({
            width: 'element',
            placeholder: "Toutes Editions",
            allowClear: true
        });
        $('#ModelvalidationModelsectionId').select2({
            width: 'element',
            placeholder: "Selectionner une section",
            allowClear: true
        });
        $('#ModelvalidationModelvariableId').select2({
            width: 'element',
            placeholder: "Selectionner une variable",
            allowClear: true
        });

        $('#ModelvalidationActiveMin').change(function () {
            if ($('#ModelvalidationActiveMin').prop('checked')) {
                /* Min activé */
                $('#ModelvalidationMin').closest('div').show();
                $('#ModelvalidationMin').val(1);
            } else {
                /* Min désactivé */
                $('#ModelvalidationMin').closest('div').hide();
                $('#ModelvalidationMin').val(0);
            }
        });

        $('#ModelvalidationMin').change(function () {
            if ($('#ModelvalidationMin').val() < 0)
                $('#ModelvalidationMin').val(0);
            if ($('#ModelvalidationActiveMax').prop('checked') && $('#ModelvalidationMax').val() < $('#ModelvalidationMin').val()) {
                $('#ModelvalidationMax').val($('#ModelvalidationMin').val());
            }
        });

        $('#ModelvalidationActiveMax').change(function () {
            if ($('#ModelvalidationActiveMax').prop('checked')) {
                /* Max activé */
                $('#ModelvalidationMax').closest('div').show();
                if ($('#ModelvalidationMin').val() != 0)
                    $('#ModelvalidationMax').val($('#ModelvalidationMin').val());
                else
                    $('#ModelvalidationMax').val(1);
            } else {
                /* Max désactivé */
                $('#ModelvalidationMax').closest('div').hide();
                $('#ModelvalidationMax').val(0);
            }
        });

        $('#ModelvalidationMax').change(function () {
            if ($('#ModelvalidationMax').val() < 0)
                $('#ModelvalidationMax').val(0);
            if ($('#ModelvalidationActiveMin').prop('checked') && $('#ModelvalidationMax').val() < $('#ModelvalidationMin').val()) {
                $('#ModelvalidationMin').val($('#ModelvalidationMax').val());
            }
        });

        /* Génération bloc sql */


        $('#ModelvalidationModeltypeId').change(generateSql);
        $('#ModelvalidationModelsectionId').change(generateSql);
        $('#ModelvalidationModelvariableId').change(generateSql);
        $("#ModelvalidationMin").change(generateSql);
        $("#ModelvalidationMax").change(generateSql);
        $("#ModelvalidationActiveMin").change(generateSql);
        $("#ModelvalidationActiveMax").change(generateSql);
        $('#sql_commands').focus(function() {
            var $this = $(this);
            $this.select();
            // Work around Chrome's little problem
            $this.mouseup(function() {
                // Prevent further mouseup intervention
                $this.unbind("mouseup");
                return false;
            });
        });

    function generateSql() {
        var types = $('#ModelvalidationModeltypeId').val(),
            sections = $('#ModelvalidationModelsectionId').val(),
            variables = $('#ModelvalidationModelvariableId').val(),
            min = $("#ModelvalidationMin").val(),
            max = $("#ModelvalidationMax").val(),
            text = '',
            $sql = $('#sql_commands');

        if (variables != null)
            $.each(variables, function (v, variable) {
                if (sections != null)
                    $.each(sections, function (s, section) {
                        if (types != null)
                            $.each(types, function (s, type) {
                                text += getInsert(variable, section, type, min, max);
                            });
                        else
                            text += getInsert(variable, section, 1, min, max);
                    });
            });

        $($sql).val(text);

        if (text != '')
            $($sql).show();
        else
            $($sql).hide();

    }

    function getInsert(variable, section, type, min, max) {
        return "INSERT INTO modelvalidations (modelvariable_id, modelsection_id, modeltype_id, min, max, actif) VALUES (" + variable + ", " + section + ", " + type + ", " + min + ", " + max + ", true);\n";
    }
        });
});
</script>

<style>
    #ModelvalidationModeltypeId, #ModelvalidationModelsectionId, #ModelvalidationModelvariableId {
        width: 400px;
    }

    div.input.select {
        margin: 15px;
    }

    #sql_commands {
        display: none;
        font-family: "courier new", courier, typewriter, monospace;
        font-size: 10px;
        width: 100%;
        line-height: 14px;
    }
</style>
