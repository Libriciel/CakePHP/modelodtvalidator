<?php
$this->Html->addCrumb(__('Règles de validation des modèles'), ['admin' => true,'prefix'=> 'admin', 'plugin'=>'model_odt_validator', 'controller'=>'modelvalidations', 'action'=>'index']);
$this->Html->addCrumb(__('Modification de la règle de validations'));

echo $this->Bs->tag('h3', __('Modifier la règle de validation')) ;


echo $this->BsForm->create('Modelvalidation', ['url' => [], 'type' => 'post']);
echo $this->BsForm->select('modeltype_id', $types, [
    'label' => 'Type',
    'class' => 'selectone',
    'empty' => false,
    'data-placeholder'=> "Toutes Editions"]);
echo $this->BsForm->select('modelsection_id', $sections, [
    'label' => 'Section',
    'class' => 'selectone',
    'empty' => false,
    'data-placeholder'=> "Document"]);
echo $this->BsForm->checkbox('activeMin', ['label' => 'Fixer un nombre minimum', 'value'=>false]);
echo $this->BsForm->input('min', ['label' => 'Minimum', 'type'=>'number', 'value'=>'0', 'div'=>['style'=>'display:none']]);
echo $this->BsForm->checkbox('activeMax', ['label' => 'Fixer un nombre maximum','type'=>'number', 'value'=>false]);
echo $this->BsForm->input('max', ['label' => 'Maximum', 'value'=>'0', 'div'=>['style'=>'display:none']]);
echo $this->Html2->btnSaveCancel('', $previous);
echo $this->BsForm->end();
?>
<script type="application/javascript">
require(['domReady'], function (domReady) {
    domReady(function () {

        $('#ModelvalidationActiveMin').change(function() {
            if ($(this).prop('checked')) {
                /* Min activé */
                $(this).closest('div').show();
                $(this).val(1);
            } else {
                /* Min désactivé */
                $(this).closest('div').hide();
                $(this).val(0);
            }
        });

        $('#ModelvalidationMin').change(function() {
            if ($(this).val() < 0)
                $(this).val(0);
            if ($('#ModelvalidationActiveMax').prop('checked') && $(this).val() < $('#ModelvalidationMin').val()){
                $(this).val($('#ModelvalidationMin').val());
            }
        });

        $('#ModelvalidationActiveMax').change(function() {
            if ($(this).prop('checked')){
                /* Max activé */
                $('#ModelvalidationMax').closest('div').show();
                if ($('#ModelvalidationMin').val() != 0)
                    $('#ModelvalidationMax').val($('#ModelvalidationMin').val());
                else
                    $('#ModelvalidationMax').val(1);
            }else{
                /* Max désactivé */
                $('#ModelvalidationMax').closest('div').hide();
                $('#ModelvalidationMax').val(0);
            }
        });

        $('#ModelvalidationMax').change(function(){
            if ($(this).val() < 0){
                $(this).val(0);
            }
            if ($('#ModelvalidationActiveMin').prop('checked') && $(this).val() < $('#ModelvalidationMin').val()){
                $('#ModelvalidationMin').val($(this).val());
            }
        });

        <?php if (!empty($this->request->data['Modelvalidation']['min'])) : ?>
        $('#ModelvalidationActiveMin').click();
        $('#ModelvalidationMin').val(<?php echo $this->request->data['Modelvalidation']['min']; ?>);
        <?php endif; ?>

        <?php if (!empty($this->request->data['Modelvalidation']['max'])) : ?>
        $('#ModelvalidationActiveMax').click();
        $('#ModelvalidationMax').val(<?php echo $this->request->data['Modelvalidation']['max']; ?>);
        <?php endif; ?>
        });
});
</script>
