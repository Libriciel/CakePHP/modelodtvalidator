<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Maintenance'));
$this->Html->addCrumb(__('Règles de validation des modèle'));

echo $this->Bs->tag('h3', __('Règles de validation des modèles')) ;
echo $this->Bs->alert(__('<strong>Attention!</strong> Les modifications apportées aux règles de validation peuvent créer des problèmes lors de
    l\'ajout des modèles d\'édition, cette page de configuration est destinée uniquement aux utilisateurs avertis'), 'danger');

echo $this->Form->create('Modelvalidation', ['url'=> ['action' => 'index']]);

echo $this->Bs->btn($this->Bs->icon('filter') . ' ' . 'Filtrer', '#', [
    'type' => 'primary', 'id' => 'filtrer', 'class' => 'btn', 'data-toggle' => 'button', 'escapeTitle' => false]);

echo $this->Html->tag('hr');
echo $this->Html->tag('div', null, ['style' => 'display:none', 'id' => 'divFiltres']);
echo $this->Form->input('Filtre.modeltype_id', ['label' => 'Type', 'placeholder' => 'Type de modèle', 'options' => $types, 'empty' => true]);
echo '<br />';
echo $this->Form->input('Filtre.modelsection_id', ['label' => 'Section', 'placeholder' => 'Nom de la section', 'options' => $sections, 'empty' => true]);
echo '<br />';
echo $this->Form->input('Filtre.modelvariable_id', ['label' => 'Variable', 'empty' => 'Ne concerne pas une variable', 'options' => $variables, 'empty' => true]);
echo '<br />';
echo $this->Form->button($this->Bs->icon('filter') .' '.__('Appliquer le filtre'), ['class' => 'btn btn-info', 'escapeTitle' => false, 'type' => 'submit']);

echo $this->Html->tag('hr');

echo $this->Html->tag('/div', null);

echo $this->Form->end();

$barreActions = $this->Html->tag('div', null, ['class' => 'btn-group']);
$barreActions .= $this->Html->link($this->Bs->icon('flag') .' '.__('Ajouter des règles'), ['action' => 'generate'], ['escapeTitle' => false, 'class' => 'btn btn-primary']);
$barreActions .= $this->Html->link($this->Bs->icon('code') .' '.__('Variables'), ['controller' => 'Modelvariables'], ['escapeTitle' => false, 'class' => 'btn btn-info']);
$barreActions .= $this->Html->link($this->Bs->icon('flag-checkered') .' '.__('Sections'), ['controller' => 'Modelsections'], ['escapeTitle' => false, 'class' => 'btn btn-info']);
$barreActions .= $this->Html->tag('/div', null);
echo $barreActions;
?>
<hr/>
<table class="table table-striped">
    <caption>Liste des règles de validation par type de modèle (variables et sections autorisées)</caption>
    <thead>
    <tr>
        <th>Type</th>
        <th>Section</th>
        <th>Variable</th>
        <th>Nombre</th>
        <th style="width: 100px">Actions</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($datas as $rel) {
        if (empty($rel['Modelvalidation']['actif'])) {
            echo $this->Html->tag('tr', null, ['class' => 'error']);
        } else {
            echo $this->Html->tag('tr', null);
        }
        echo $this->Html->tag('td', $rel['Modeltype']['name']);
        echo $this->Html->tag('td', $rel['Modelsection']['name']);
        echo $this->Html->tag('td', $rel['Modelvariable']['name']);

        // Nombre
        if (empty($rel['Modelvalidation']['min']) && empty($rel['Modelvalidation']['max'])) {
            echo $this->Html->tag('td', 'Non défini');
        } elseif ($rel['Modelvalidation']['min'] === $rel['Modelvalidation']['max']) {
            echo $this->Html->tag('td', 'Exactement : ' . $rel['Modelvalidation']['min']);
        } elseif (!empty($rel['Modelvalidation']['min']) && !empty($rel['Modelvalidation']['max'])) {
            echo $this->Html->tag('td', 'Entre ' . $rel['Modelvalidation']['min'] . ' et ' . $rel['Modelvalidation']['max']);
        } elseif (!empty($rel['Modelvalidation']['max'])) {
            echo $this->Html->tag('td', 'Maximum : ' . $rel['Modelvalidation']['max']);
        } elseif (!empty($rel['Modelvalidation']['min'])) {
            echo $this->Html->tag('td', 'Minimum : ' . $rel['Modelvalidation']['min']);
        }

        echo $this->Html->tag(
            'td',
            $this->Html->tag('div', null, ['class' => 'btn-group'])
            .
            $this->Bs->btn($this->Bs->icon('pencil-alt'), ['action' => 'edit', $rel['Modelvalidation']['id']], [
                'type' => 'primary',
                'escapeTitle' => false,
                'title' => 'Modifier la règle'])
            .
            $this->Bs->btn($this->Bs->icon('trash'), ['action' => 'delete', $rel['Modelvalidation']['id']], [
                'type' => 'danger',
                'escapeTitle' => false,
                'title' => __('Supprimer la règle'),
                'confirm'=> __('Confirmez-vous la suppression de cette règle de validation ? Attention cette action est irréversible !')
                ])
            .
            $this->Html->tag('/div', null),
            ['style' => 'text-align:center;']
        );
        echo $this->Html->tag('/tr', null);
    }
    ?>
    </tbody>
</table>
<script type="application/javascript">
require(['domReady'], function (domReady) {
    domReady(function () {
        $('#filtrer').click(function () {
            if ($('#divFiltres').is(':visible'))
                $('#divFiltres').hide();
            else
                $('#divFiltres').show();
        });

        $('#FiltreModeltypeId').select2({
            width: 'element',
            placeholder: "Filtre de type",
            allowClear: true
        });
        $('#FiltreModelsectionId').select2({
            width: 'element',
            placeholder: "Filtre de section",
            allowClear: true
        });
        $('#FiltreModelvariableId').select2({
            width: 'element',
            placeholder: "Filtre de variable",
            allowClear: true
        });

        <?php if (!empty($this->request->data['Filtre'])) : ?>
        $('#divFiltres').show();
        $('#filtrer').addClass('active');
        <?php endif; ?>

        });
});
</script>
