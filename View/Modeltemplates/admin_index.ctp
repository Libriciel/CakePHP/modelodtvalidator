<?php

/**
 * web-delib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib web-delib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Générale'));

$this->Html->addCrumb('Liste des modèles d\'édition');

echo $this->Bs->tag('h3', 'Liste des modèles d\'édition');
if ($this->permissions->check('Modeltemplates', 'create')) {
    echo $this->Bs->btn(
        'Ajouter un modèle',
        [
      'admin'=>true,
      'prefix'=>'admin',
      'plugin'=>'model_odt_validator',
      'controller'=>'modeltemplates',
      'action' => 'add'],
        ['escape' => false,
              'icon'=>'plus-circle',
              'type' => 'success',
              'title' => __('Ajouter un nouveau modèle d\'édition')]
    );
}

echo $this->Bs->table([
    ['title' => 'Nom'],
    ['title' => 'Type'],
    ['title' => 'Nom du fichier'],
    ['title' => 'Format de sortie'],
    ['title' => 'Date de modification'],
    ['title' => 'Actions', 'width' => '3b'],
        ], ['hover', 'striped'], ['caption'=>'Liste des modèles d\'édition pour la génération de documents']);

    foreach ($templates as $template) {
        echo $this->Bs->cell($template['Modeltemplate']['name']);
        echo $this->Bs->cell($template['Modeltype']['name']);
        echo $this->Bs->cell($template['Modeltemplate']['filename']);
        echo $this->Bs->cell($template['Modeltemplate']['file_output_format']);
        echo $this->Bs->cell($template['Modeltemplate']['modified']);

        echo $this->Bs->cell(
            $this->Bs->div('btn-group btn-group-justified') .
        ($this->permissions->check('Modeltemplates', 'read')?
$this->Bs->btn(
    '',
    [
    'admin'=>true,
    'prefix'=>'admin',
    'plugin'=>'model_odt_validator',
    'controller'=>'modeltemplates',
    'action' => 'view', $template['Modeltemplate']['id']],
    [
            'icon'=>'info',
            'type' => 'default',
            'title' => __('Plus d\'informations')]
):'').
($this->permissions->check('Modeltemplates', 'update')?
$this->Bs->btn(
    '',
    [
    'admin'=>true,
    'prefix'=>'admin',
    'plugin'=>'model_odt_validator',
    'controller'=>'modeltemplates',
    'action' => 'edit', $template['Modeltemplate']['id']],
    [
            'icon'=>'pencil-alt',
            'type' => 'primary',
            'title' => __('Modifier le modèle')]
):'').
($this->permissions->check('Modeltemplates', 'delete')?
$this->Bs->btn(
    '',
    [
    'admin'=>true,
    'prefix'=>'admin',
    'plugin'=>'model_odt_validator',
    'controller'=>'modeltemplates',
    'action' => 'delete', $template['Modeltemplate']['id']],
    [
            'icon'=>'trash',
            'type' => 'danger',
            'confirm'=> __('Confirmez vous la suppression du modèle d\'édition ?'),
            'title' => __('Supprimer le modèle')]
):'').$this->Bs->close()
        );
    }
echo $this->Bs->endTable();
echo $this->Bs->div('btn-group').$this->Html2->btnCancel();
if ($canEditRules) {
    echo $this->Bs->btn(
        'Règles de validation',
        [
    'admin'=>true,
    'prefix'=>'admin',
    'plugin'=>'model_odt_validator',
    'controller'=>'modelvalidations',
    'action' => 'index'],
        [
            'icon'=>'check',
            'type' => 'warning',
            'title' => __('Règles de validation des modèles')]
    );
}
echo $this->Bs->close();
