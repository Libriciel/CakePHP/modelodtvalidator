<?php

/**
 * web-delib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib web-delib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Générale'));
$this->Html->addCrumb(__('Liste des modèles d\'édition'), ['admin' => true,'prefix'=> 'admin', 'plugin'=>'model_odt_validator', 'controller'=>'modeltemplates', 'action'=>'index']);
$this->Html->addCrumb(__('%s', $this->data['Modeltemplate']['name']));

echo
 $this->Bs->panel($this->Bs->tag('h4', __('Modèle d\'édition : %s (Type : %s)', $this->data['Modeltemplate']['name'], $this->data['Modeltype']['name'])))   .
 $this->Bs->row() .
 $this->Bs->col('xs6') .
        $this->Bs->tag('p', 'Nom : ' . $this->data['Modeltemplate']['name']) .
        $this->Bs->tag('p', 'Type de modèle : ' . $this->data['Modeltype']['name']) .
        $this->Bs->tag('p', 'Description : ' . $this->data['Modeltype']['description']) .
        $this->Bs->tag('p', __('Forcer le modèle : %s', $this->data['Modeltemplate']['force'] ? 'Oui' : 'Non')) .
        $this->Bs->tag('p', 'Nommage du fichier de sortie : ' . $this->data['Modeltemplate']['file_output_format']) .
        $this->Bs->tag('p', 'Fichier : ' . $this->Html->link(
            $this->data['Modeltemplate']['filename'] . ' ' . $this->Bs->icon('download'),
            ['action'=>'download', $this->data['Modeltemplate']['id']],
            [
                    'escapeTitle'=>false]
        )
                .' '. $this->data['Modeltemplate']['filesize']) .
        $this->Bs->tag('p', 'Date de création : ' . $this->data['Modeltemplate']['created']) .
        $this->Bs->tag('p', 'Date de modification : ' . $this->data['Modeltemplate']['modified']) .
 $this->Bs->close();
 echo $this->Bs->col('xs6');

    echo $this->Bs->tag('h5', __('Vérifications'));
    $li='';
     if (!empty($validation['errors'])) {
         foreach ($validation['errors'] as $error) {
             $li .= $this->Bs->tag('li', $error, ['class'=>'list-group-item list-group-item-danger']);
         }
     }
    if (!empty($validation['warnings'])) {
        foreach ($validation['warnings'] as $warning) {
            $li .= $this->Bs->tag('li', $warning, ['class'=>'list-group-item list-group-item-warning']);
        }
    }
    if (empty($li)) {
        $li .= $this->Bs->tag('li', __('Modèle valide'), ['class'=>'list-group-item list-group-item-success']);
    }

    echo $this->Bs->tag('ul', $li, ['class'=>'list-group']);

echo $this->Bs->close(2) .
 $this->Bs->endPanel() .
 $this->Bs->row() .
 $this->Bs->col('md4 of5') .

 $this->Bs->div('btn-group', null, ['id' => "actions_fiche"]) .
 $this->Html2->btnCancel() .
 $this->Bs->btn($this->Bs->icon('pencil-alt') . ' ' . __('Modifier'), [
     'controller' => 'modeltemplates',
     'action' => 'edit', $this->data['Modeltemplate']['id']], [
         'type' => 'primary',
         'title' => __('Modifier le modèle'),
         'escapeTitle' => false]) .
$this->Bs->close(3);
