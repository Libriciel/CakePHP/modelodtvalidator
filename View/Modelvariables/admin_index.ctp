<?php
$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Maintenance'));
$this->Html->addCrumb(__('Règles de validation des modèles'), ['admin' => true,'prefix'=> 'admin', 'plugin'=>'model_odt_validator', 'controller'=>'modelvalidations', 'action'=>'index']);
$this->Html->addCrumb('Variables de modèles');

echo $this->Bs->tag('h3', __('Variables de modèles')) ;

echo $this->Form->create('Modelvariable', ['url'=>['action' => 'index']]);
echo $this->Form->input('nom', ['label' => ['text' => 'Rechercher une variable', 'style' => 'width: auto; padding-top: 5px;'], 'placeholder' => 'Nom de variable', 'options' => $names, 'empty' => true, 'div' => false]);
echo '<br />';
echo $this->Form->button('<i class="fa fa-filter"></i> Appliquer le filtre', ['class' => 'btn btn-info', 'escape' => false, 'type' => 'submit', 'style' => 'margin-left:5px']);
echo $this->Html->tag('hr');
echo $this->Form->end();

$barreActions = $this->Html->tag('div', null, ['class' => 'btn-group']);
$barreActions .= $this->Bs->btn($this->Bs->icon('code').' '.'Ajouter une variable', ['action' => 'add'], ['escapeTitle' => false, 'type' => 'primary', 'title' => 'Déclarer une nouvelle variable de modèle']);
$barreActions .= $this->Bs->btn($this->Bs->icon('flag').' '.'Règles de validation', ['controller' => 'Modelvalidations'], ['escapeTitle' => false, 'type' => 'info', 'title' => 'Règles de validation des modèles d\'édition']);
$barreActions .= $this->Bs->btn($this->Bs->icon('flag-checkered').' '.'Sections', ['controller' => 'Modelsections'], ['escapeTitle' => false, 'type' => 'info', 'title' => 'Liste des sections de modèle']);
$barreActions .= $this->Html->tag('/div', null);

echo $barreActions;
?>
<hr/>
<table class="table table-striped">
    <caption>Liste des variables autorisées dans les modèles</caption>
    <thead>
    <tr>
        <th>id</th>
        <th>Nom</th>
        <th>Description</th>
        <th>Création</th>
        <th>Modification</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($data as $variable) {
        echo $this->Html->tag('tr', null);

        echo $this->Html->tag('td', $variable['Modelvariable']['id']);
        echo $this->Html->tag('td', $variable['Modelvariable']['name']);
        echo $this->Html->tag('td', $variable['Modelvariable']['description']);
        echo $this->Html->tag('td', date('d/m/Y \à h:i:s', strtotime($variable['Modelvariable']['created'])));
        echo $this->Html->tag('td', date('d/m/Y \à h:i:s', strtotime($variable['Modelvariable']['modified'])));

        echo $this->Html->tag(
            'td',
            $this->Bs->div('btn-group')
            .
            $this->Bs->btn($this->Bs->icon('pencil-alt'), ['action' => 'edit', $variable['Modelvariable']['id']], ['type' => 'primary', 'escapeTitle' => false, 'title' => 'Modifier la règle'])
            .
            $this->Bs->btn($this->Bs->icon('trash'), ['action' => 'delete', $variable['Modelvariable']['id']], ['type' => 'danger', 'escapeTitle' => false, 'title' => 'Supprimer la règle'], "Confirmez-vous la suppression de cette règle de validation ? Attention cette action est irréversible !")
            .
            $this->Bs->close()
        );
        echo $this->Html->tag('/tr', null);
    }
    ?>
    </tbody>
</table>
<hr/>
<?php
echo $barreActions;
?>
<script type="application/javascript">
require(['domReady'], function (domReady) {
    domReady(function () {
        $('#filtrer').change(function () {
            if ($('#filtrer').prop('checked'))
                $('#divFiltres').show();
            else
                $('#divFiltres').hide();
        });

        $('#ModelvariableNom').select2({
            width: 'element',
            placeholder: "Nom de variable",
            allowClear: true
        });

        <?php if (!empty($this->request->data['Modelvariable']['filtrer'])) : ?>
        $('#divFiltres').show();
        <?php endif; ?>

        });
});
</script>
